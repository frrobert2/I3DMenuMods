# I3DMenuMods

This package gives you a menu system that uses either dmenu or fzf.
dmenu_recency is taken from packages-community/dmenu-manjaro
fzf_recency is a work in progress.  It is a shell script designed to use fzf rather than dmenu.

The easiest way to use it is first add the following to your i3 config file

bindsym $mod+m exec --no-startup-id fzf_recency

bindsym $mod+d exec --no-startup-id dmenu_recency

for_window [title="FZFMENU"] floating enable

for_window [title="FZFMENU"] resize set 800 600

for_window [title="FZFMENU"] move position center

for_window [title="FZFMENU"] move to scratchpad



A menu of available programs will come up.

The first time you select a program you will be asked how you want to run it.

Once you select the way you want to run the program the program will start and the menu will go back to the scratch pad.

Hit the hot key again to bring the window back and you will be presented with the menu.

If you want to change how a program is run go to .config/dmenu-recent and edit either the background or terminal file and remove the incorrect entry.


.dmenurc is a configuration file that is required by both dmenu_recency and fzf_recency.

edit .dmenurc to tell both programs which terminal emulator you want to use.  Also it contains other config items for dmenu.





